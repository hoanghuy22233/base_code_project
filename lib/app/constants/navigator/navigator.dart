import 'package:base_code_project/presentation/change_info_verify/bloc/change_info_verify_bloc.dart';
import 'package:base_code_project/presentation/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.offAllNamed(BaseRouter.NAVIGATION);
    return result;
  }
  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD);
    return result;
  }
  static navigateForgotPasswordVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }
  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
    await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }
  static navigateRegisterVerify({String username}) async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY,
        arguments: {'username': username});
    return result;
  }
  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }
  static navigateProfileDetail() async {
    var result = await Get.toNamed(BaseRouter.PROFILE_DETAIL);
    return result;
  }
  static navigateChangeInfoVerify({String username, ChangeInfoVerifyType type}) async {
    var result = await Get.toNamed(BaseRouter.CHANGE_INFO_VERIFY, arguments: {
      'username': username,
      'type' : type
    });
    return result;
  }
  static navigateRank() async {
    var result = await Get.toNamed(BaseRouter.LIST_PROFILE);
    return result;
  }
  static navigateContact() {
    Get.toNamed(BaseRouter.CONTACT);
  }
  static navigateTerm() async {
    var result = await Get.toNamed(BaseRouter.TERM);
    return result;
  }
  static navigateAddProject() async {
    var result = await Get.toNamed(BaseRouter.ADD_PROJECT);
    return result;
  }
  static navigateFullProject() async {
    var result = await Get.toNamed(BaseRouter.FULL_PROJECT);
    return result;
  }

  // static navigateAddFullProject() async {
  //   var result = await Get.toNamed(BaseRouter.ADD_FULL_PROJECT);
  //   return result;
  // }
}
