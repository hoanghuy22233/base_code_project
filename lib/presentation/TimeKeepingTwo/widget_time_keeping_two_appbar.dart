import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home_bell.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home_page.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_add.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back_two.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_time_keeping_page.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetTimeKeepingTwoAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarTimeKeepingPage(
        title: AppLocalizations.of(context).translate('timekeeping.appbar').toUpperCase(),
        // left: [WidgetAppbarMenuBackTwo()],
        right: [WidgetAppbarHomeBell()],
      ),
    );
  }
}
