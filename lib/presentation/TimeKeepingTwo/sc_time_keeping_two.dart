
import 'package:base_code_project/presentation/TimeKeepingTwo/widget_time_keeping_two.dart';
import 'package:base_code_project/presentation/TimeKeepingTwo/widget_time_keeping_two_appbar.dart';
import 'package:flutter/material.dart';

class TimeKeepingTwoScreen extends StatefulWidget {
  @override
  TimeKeepingTwoState createState() => TimeKeepingTwoState();
}

class TimeKeepingTwoState extends State<TimeKeepingTwoScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: _buildCondition(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingTwoAppbar();

  Widget _buildCondition() => WidgetTimeKeepingTwo();
}
