import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_project_full.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_time_sheet.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/menu/home/list_home.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';

class WidgetTimeKeepingTwo extends StatefulWidget {
  const WidgetTimeKeepingTwo({
    Key key,
  }) : super(key: key);

  @override
  _WidgetTimeKeepingTwoState createState() => _WidgetTimeKeepingTwoState();
}

class _WidgetTimeKeepingTwoState extends State<WidgetTimeKeepingTwo>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height/15,
            margin: EdgeInsets.symmetric(horizontal: 17),
            child: Row(
              children: [
                Image.asset(
                  "assets/images/img_search.png",
                  width: 20,
                  height: 20,
                ),
                SizedBox(width: 5,),
                Expanded(
                  flex: 8,
                  child: TextField(
                    decoration: new InputDecoration.collapsed(
                      // border: InputBorder.none,
                        hintText: 'Tìm kiếm'),
                  ),
                  // child:TextFormField(
                  //   cursorColor: Colors.black,
                  //   // keyboardType: inputType,
                  //   decoration: new InputDecoration(
                  //       border: InputBorder.none,
                  //       focusedBorder: InputBorder.none,
                  //       enabledBorder: InputBorder.none,
                  //       errorBorder: InputBorder.none,
                  //       disabledBorder: InputBorder.none,
                  //       contentPadding:
                  //       EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                  //       hintText: "tìm kiếm"),
                  // )
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 3,
            // color: Colors.grey[300],
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 7,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height-90,
            child: ListView.builder(
              physics:  AlwaysScrollableScrollPhysics(),
              itemCount: dataTimeSheet.length,
              itemBuilder: (context, index) {
                return Container(
                  // width: MediaQuery.of(context).size.width,
                  // height: MediaQuery.of(context).size.height/6,
                  // margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),

                  // color: Colors.blue,
                  // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 17, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${dataTimeSheet[index].textHeadquarters}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                            SizedBox(height: 3,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  "assets/images/ic_location.png",
                                  width: 20,
                                  height: 20,
                                ),
                                SizedBox(width: 5,),
                                Text("${dataTimeSheet[index].textlocated}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14)),
                              ],
                            ),
                            SizedBox(height: 3,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  "assets/images/ic_time_sheet_back.png",
                                  width: 25,
                                  height: 25,
                                ),
                                SizedBox(width: 5,),
                                Text(
                                  "${dataTimeSheet[index].textTimeSheet}",
                                  style: TextStyle(color: Colors.green,fontWeight: FontWeight.bold,fontSize: 14),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1,
                        color: Colors.grey,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 1,
                        color: Colors.grey,
                      )
                    ],
                  ),
                );
              },
            ),
          ),
          // Container(
          //   height: 65,
          // )


        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
