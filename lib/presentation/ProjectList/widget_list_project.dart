import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';

class WidgetListProject extends StatefulWidget {
  const WidgetListProject({
    Key key,
  }) : super(key: key);

  @override
  _WidgetProjectState createState() => _WidgetProjectState();
}

class _WidgetProjectState extends State<WidgetListProject>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Padding(
        //   padding: const EdgeInsets.symmetric(vertical: 20),
        //   child: Text(
        //     AppLocalizations.of(context)
        //         .translate('profile.list')
        //         .toUpperCase(),
        //     style: AppStyle.DEFAULT_MEDIUM_BOLD,
        //   ),
        // ),
        Stack(
          children: [
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Divider(
                  color: AppColor.GREY,
                  thickness: 0.4,
                  height: 1,
                ),
              ),
            ),
            TabbarHeader(
              controller: controller,
              // backgroundColor: Color(0xff150ee6) ,
              backgroundColor: Colors.white,
              tabs: [
                Tab(
                    child: Column(
                  children: [
                    Image.asset("assets/images/project_finish.png",width: 20,height: 20,),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Đã làm")
                  ],
                )),
                Tab(
                    child: Column(
                  children: [
                    Image.asset("assets/images/doing_project.png",width: 20,height: 20,),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Đang làm")
                  ],
                )),
                Tab(
                    child: Column(
                  children: [
                    Image.asset("assets/images/delete_red.png",width: 20,height: 20,),
                    SizedBox(
                      height: 7,
                    ),
                    Text("Đã Hủy")
                  ],
                )),
              ],
            ),
          ],
        ),
        Container(
          height: 600,
          // child: TabBarView(controller: _tabController, children: _buildTabBarView(widget.rankData.ranking)),
          child: TabbarContent(
            controller: controller,
            children: <Widget>[
              Container(color: Colors.yellow),
              Container(color: Colors.red),
              Container(color: Colors.purple),
            ],
          ),
        ),
      ],
    ));
  }
}
