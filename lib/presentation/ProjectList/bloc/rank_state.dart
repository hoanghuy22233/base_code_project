import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/utils/utils.dart';
import 'package:equatable/equatable.dart';

abstract class RankState extends Equatable {
  const RankState();

  @override
  List<Object> get props => [];
}

class RankLoading extends RankState {}

class RankLoaded extends RankState {
  final RankData rankData;

  const RankLoaded(this.rankData);

  @override
  List<Object> get props => [rankData];

  @override
  String toString() {
    return 'RankLoaded{rankData: $rankData}';
  }
}

class RankNotLoaded extends RankState {
  final DioStatus status;

  RankNotLoaded(this.status);

  @override
  String toString() {
    return 'RankNotLoaded{status: $status}';
  }
}