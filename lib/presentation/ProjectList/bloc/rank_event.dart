import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RankEvent extends Equatable {
  const RankEvent();

  List<Object> get props => [];
}

class LoadRank extends RankEvent {
}

class RefreshRank extends RankEvent {}
