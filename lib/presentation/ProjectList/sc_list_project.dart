import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/ProjectList/widget_list_projact_appbar.dart';
import 'package:base_code_project/presentation/ProjectList/widget_list_project.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/rank_bloc.dart';
import 'bloc/rank_event.dart';
import 'bloc/rank_state.dart';

class ListProject extends StatefulWidget {
  @override
  ListProjectScreenState createState() => ListProjectScreenState();
}

class ListProjectScreenState extends State<ListProject> {
  @override
  void initState() {
    super.initState();
    // BlocProvider.of<RankBloc>(context).add(LoadRank());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: _buildContent(),
                ),
              ),

              // child: BlocBuilder<RankBloc, RankState>(
              //   builder: (context, state) {
              //     return RefreshIndicator(
              //       onRefresh: () async {
              //         BlocProvider.of<RankBloc>(context).add(RefreshRank());
              //         await Future.delayed(AppValue.FAKE_TIME_RELOAD);
              //         return true;
              //       },
              //       color: AppColor.PRIMARY_COLOR,
              //       backgroundColor: AppColor.THIRD_COLOR,
              //       child: SingleChildScrollView(
              //         child: Container(
              //           child: _buildContent(state),
              //         ),
              //       ),
              //     );
              //   },
              // ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Column(
      children: [
        _buildCondition()
      ],
    );
  }

  Widget _buildAppbar() => WidgetListProjectAppbar();


  Widget _buildCondition() => WidgetListProject(
  );
}

