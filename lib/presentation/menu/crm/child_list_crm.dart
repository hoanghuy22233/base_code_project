import 'package:base_code_project/model/date_not_api/data_not_api_crm.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_monitorings.dart';
import 'package:base_code_project/presentation/menu/monitoring/sc_monitoring.dart';
import 'package:base_code_project/presentation/menu/sc_schedule.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ChildListCrm extends StatelessWidget {
  final int id;
  ChildListCrm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        switch (dataCrm[id].id)
        {
          case 0:
            // return(
            //     showMaterialModalBottomSheet(
            //       context: context,
            //       builder: (context) => MonitoringPageScreen(),
            //     )
            // );
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 1:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 2:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 3:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 4:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 5:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 6:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 7:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
          case 8:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SchedulePageScreen()),
            ));
            break;
        // case 9:
        //   return (
        //       Navigator.pop(context)
        //   );
        //   break;
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "${dataCrm[id].img}",
                    width: 20,
                    height: 20,
                  )),
              Expanded(
                  flex: 9,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text("${dataCrm[id].textForm}"),
                  ))
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),

    );
  }
}
