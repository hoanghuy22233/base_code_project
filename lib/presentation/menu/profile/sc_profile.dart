import 'package:base_code_project/app/auth_bloc/bloc.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_profile_menu_contact.dart';
import 'package:base_code_project/presentation/common_widgets/widget_profile_menu_rules.dart';
import 'package:base_code_project/presentation/common_widgets/widget_profile_menu_setting.dart';
import 'package:base_code_project/presentation/menu/profile/widget_profile_appbar.dart';
import 'package:base_code_project/presentation/menu/profile/widget_profile_infor.dart';
import 'package:base_code_project/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

import 'bloc/profile_bloc.dart';
import 'bloc/profile_event.dart';
import 'bloc/profile_state.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with AutomaticKeepAliveClientMixin<ProfileScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<ProfileBloc, ProfileState>(
      listener: (context, state) async {
        if (state is ProfileNotLoaded) {
          await HttpHandler.resolve(status: state.error);
        }
      },
      child: BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            body: Container(
                color: AppColor.PRIMARY_COLOR,
                child: _buildContent(state)),
          ),
        );
      }),
    );
  }

  _buildContent(ProfileState state) {
    if (state is ProfileLoaded) {
      return Column(
        children: [_buildAppbar(), _buildMenu(state)],
      );
    } else if (state is ProfileLoading) {
      return Center(
        child: WidgetCircleProgress(),
      );
    } else if (state is ProfileNotLoaded) {
      return Center(
        child: Text('${state.error}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _buildAppbar() => WidgetProfileAppbar();

  Widget _buildMenu(ProfileLoaded state) {
    return Expanded(
      child: RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          await Future.delayed(Duration(seconds: 3));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(21.0),
            child: Column(
              children: [
                WidgetProfileInfor(
                  name: state.user.name ??
                      AppLocalizations.of(context).translate('profile.no_name'),
                  phone: state.user.phoneNumber ??
                      AppLocalizations.of(context)
                          .translate('profile.no_setup'),
                  avatar: WidgetCachedImage(
                    url: state.user.avatar,
                  ),
                  // onTap: () {
                  //   AppNavigator.navigateProfileDetail();
                  // },
                ),
                WidgetProfileMenu(
                  text: AppLocalizations.of(context).translate('account_information.list'),
                  // image: Image.asset('assets/images/img_rank.png'),
                  // onTap: () {
                  //   AppNavigator.navigateRank();
                  // },
                  onTap: () {
                    AppNavigator.navigateProfileDetail();
                  },
                ),
                WidgetProfileMenuContact(
                  text:
                      AppLocalizations.of(context).translate('profile.contact'),
                  image: Image.asset('assets/images/img_contact.png'),
                  onTap: () {
                    AppNavigator.navigateContact();
                  },
                ),
                WidgetProfileMenuRules(
                  text: AppLocalizations.of(context)
                      .translate('profile.term_and_policy_two'),
                  image: Image.asset('assets/images/img_term_and_policy.png'),
                  onTap: () {
                    AppNavigator.navigateTerm();
                  },
                ),
                // WidgetProfileMenu(
                //   text: AppLocalizations.of(context)
                //       .translate('profile.change_password'),
                //   image: Image.asset('assets/images/img_change_password.png'),
                //   onTap: () {
                //     // AppNavigator.navigateChangePassword();
                //   },
                // ),
                WidgetProfileMenuSetting(
                  text:
                      AppLocalizations.of(context).translate('profile.logout'),
                  image: Image.asset('assets/images/img_logout.png'),
                  onTap: () async {
                    var result = await ConfirmDialog.show(context, AppLocalizations.of(context).translate('profile.logout.confirm'), ok: 'OK', cancel: 'Huỷ');
                    if (result==true) {
                      BlocProvider.of<AuthenticationBloc>(context)
                          .add(LoggedOut());
                      AppNavigator.navigateLogin();
                    }
                  },
                ),
                WidgetSpacer(
                  height: 100,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
