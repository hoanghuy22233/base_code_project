import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'list_monitoring.dart';

class MonitoringPageScreen extends StatefulWidget {
  MonitoringPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MonitoringPageScreenState createState() => MonitoringPageScreenState();
}

class MonitoringPageScreenState extends State<MonitoringPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/1.3 ,
      child: (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 23, vertical: 15),
                child: Text(
                  "GIÁM SÁT",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              )),
          Expanded(
            flex: 9,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: WidgetListMonitoring(),
            ),
          ),
        ],
      )),
    );
  }
}
