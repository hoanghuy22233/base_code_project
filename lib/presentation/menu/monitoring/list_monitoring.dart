import 'package:base_code_project/model/date_not_api/data_not_api_monitorings.dart';
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/menu/monitoring/child_list_monitoring.dart';
import 'package:base_code_project/presentation/menu/monitoring/sc_monitoring.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/presentation/menu/sc_schedule.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
class WidgetListMonitoring extends StatefulWidget {
  @override
  _WidgetListMonitoringState createState() => _WidgetListMonitoringState();
}

class _WidgetListMonitoringState extends State<WidgetListMonitoring> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataMonitoring.length+1,
      itemBuilder: (context, index) {
        if( index == dataMonitoring.length){
          return GestureDetector
            (
            onTap: () {
              Navigator.pop(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          "assets/images/delete_red.png",
                          width: 20,
                          height: 20,
                        )),
                    Expanded(
                        flex: 9,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Text("Hủy"),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        }else{
          return Container(
              // margin: EdgeInsets.symmetric(horizontal: 10),
        child: ChildListMonitoring(
        id: index
          ,
        ));
        }

      },
    );
  }
}


