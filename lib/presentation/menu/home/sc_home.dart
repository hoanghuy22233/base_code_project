
import 'package:base_code_project/presentation/menu/home/widget_home_page.dart';
import 'package:base_code_project/presentation/menu/home/widget_home_page_appbar.dart';

import 'package:flutter/material.dart';

class HomePageScreen extends StatefulWidget {
  @override
  HomePageScreenState createState() => HomePageScreenState();
}

class HomePageScreenState extends State<HomePageScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: _buildCondition(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetHomePageAppbar();

  Widget _buildCondition() => WidgetHomePage();
}
