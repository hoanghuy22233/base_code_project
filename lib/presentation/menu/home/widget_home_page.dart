import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/menu/home/list_home.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';

class WidgetHomePage extends StatefulWidget {
  const WidgetHomePage({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullProjectState createState() => _WidgetFullProjectState();
}

class _WidgetFullProjectState extends State<WidgetHomePage>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
              flex: 5,
              child: ListView.builder(
                itemCount: dataHome.length,
                itemBuilder: (context, index) {
                  return SingleChildScrollView(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/8,
                      // color: Colors.blue,
                      margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: ChildListHomeTwo(
                        id: index,
                      ),
                    ),
                  );
                },
              )),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
