import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'list_work.dart';

class WorkPageScreen extends StatefulWidget {
  WorkPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  WorkPageScreenState createState() => WorkPageScreenState();
}

class WorkPageScreenState extends State<WorkPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/3 ,
      child: (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 23, vertical: 10),
                child: Text(
                  "WORK+",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey
                  ),
                ),
              )),
          Expanded(
            flex: 10 ,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: WidgetListWork(),
            ),
          ),
        ],
      )),
    );
  }
}
