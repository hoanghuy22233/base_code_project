import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_bussiness.dart';
class WidgetBussiness extends StatelessWidget {
  final int id;


  WidgetBussiness({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(

        margin: EdgeInsets.all(4),
        elevation: 5,

        child: Container(
          padding: EdgeInsets.all(10),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 25,
                width: 110,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text("Làm thêm",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white)),
              ),
              SizedBox(height: 15,),

              Padding(
                padding: EdgeInsets.symmetric(vertical: 15),

                child: Column(
                  children: [
                    Text("${dataBussiness[0].Bussiness.toString()}",style: TextStyle(color: Colors.blue, fontSize: 40),),
                    Text("ngày", style: TextStyle(fontSize: 20),),
                  ],
                ),
              )



            ],
          ),
        ));
  }
}