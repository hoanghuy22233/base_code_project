import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_overtime.dart';
class WidgetOvertime extends StatelessWidget {
  final int id;


  WidgetOvertime({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(

        margin: EdgeInsets.all(4),
        elevation: 5,

        child: Container(
          padding: EdgeInsets.all(10),
          
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 25,
                width: 110,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text("Tăng ca",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white)),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,

                children: [
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    direction: Axis.vertical,
                    spacing: 15,
                    children: [
                      Text("Số công"),

                      Image.asset("assets/images/deadline.png", color: Colors.red, width: 50, height: 50,),
                      Text("${dataOverTime[0].totalOverTime.toString()} \ncông", style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                    ],
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    direction: Axis.vertical,
                    spacing: 15,
                    children: [
                      Text("Số giờ"),
                      Image.asset("assets/images/clock.png", color: Colors.red, width: 50, height: 50,),
                      Center(child: Text("${dataOverTime[0].overTimeHours} \ngiờ", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,))
                    ],
                  ),
                ],
              )


            ],
          ),
        ));
  }
}