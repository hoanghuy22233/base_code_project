import 'package:base_code_project/model/date_not_api/data_not_api_work.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_workdays.dart';
import 'package:percent_indicator/percent_indicator.dart';

class WidgetWorkDays extends StatelessWidget {
  final int id;


  WidgetWorkDays({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(

        margin: EdgeInsets.all(4),
        elevation: 5,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 25,
          width: 110,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text("Ngày công",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white)),
        ),

        CircularPercentIndicator(

          radius: 150.0,
          lineWidth: 13.0,
          animation: true,
          percent: (dataWorkDays[0].workDays/dataWorkDays[0].workMonth),
          center: new Text(
            "${dataWorkDays[0].workDays.toString()}",
            style:
            new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
          ),
          footer: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("Công tháng: ${dataWorkDays[0].workMonth.round().toString()} công", style: TextStyle(fontSize: 12),),
              Text("Số giờ: ${dataWorkDays[0].workHours.toString()} giờ",  style: TextStyle(fontSize: 12),),
            ],
          ),
          circularStrokeCap: CircularStrokeCap.round,
          progressColor: Colors.blue,
        ),

      ],
    ));
  }
}
// SfRadialGauge _getRangePointerExampleGauge(id) {
//   return SfRadialGauge(
//     axes: <RadialAxis>[
//       RadialAxis(
//           showLabels: false,
//           showTicks: false,
//           startAngle: 270,
//           endAngle: 270,
//           minimum: 0,
//           maximum: dataWorkDays[id].workMonth,
//           radiusFactor: 0.8,
//           axisLineStyle: AxisLineStyle(
//               thicknessUnit: GaugeSizeUnit.factor, thickness: 0.15),
//           annotations: <GaugeAnnotation>[
//             GaugeAnnotation(
//                 angle: 180,
//                 widget: Row(
//                   mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[
//                     Container(
//                       child: Text(
//                         '50',
//                         style: TextStyle(
//                             fontFamily: 'Times',
//                             // fontSize: isCardView ? 18 : 22,
//                             fontWeight: FontWeight.w400,
//                             fontStyle: FontStyle.italic),
//                       ),
//                     ),
//                     Container(
//                       child: Text(
//                         ' / ',
//                         style: TextStyle(
//                             fontFamily: 'Times',
//                             // fontSize: isCardView ? 18 : 22,
//                             fontWeight: FontWeight.w400,
//                             fontStyle: FontStyle.italic),
//                       ),
//                     )
//                   ],
//                 )),
//           ],
//           pointers: <GaugePointer>[
//             RangePointer(
//                 value: 50,
//                 cornerStyle: CornerStyle.bothCurve,
//                 enableAnimation: true,
//                 animationDuration: 1200,
//                 animationType: AnimationType.ease,
//                 sizeUnit: GaugeSizeUnit.factor,
//                 // Sweep gradient not supported in web
//                 // // gradient: model.isWeb
//                 //     ? null
//                 //     : const SweepGradient(
//                 //     colors: <Color>[Color(0xFF6A6EF6), Color(0xFFDB82F5)],
//                 //     stops: <double>[0.25, 0.75]),
//                 color: const Color(0xFF00A8B5),
//                 width: 0.15),
//           ]),
//     ],
//   );
// }
