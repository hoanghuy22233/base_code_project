
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:base_code_project/model/date_not_api/data_not_api_offdays.dart';
import 'package:percent_indicator/percent_indicator.dart';

class WidgetOffDays extends StatelessWidget {
  final int id;


  WidgetOffDays({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(

        margin: EdgeInsets.all(4),
        elevation: 5,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 25,
          width: 110,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text("Quỹ phép",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white)),
        ),

        CircularPercentIndicator(

          radius: 150.0,
          lineWidth: 13.0,
          animation: true,
          percent: (dataOffDays[0].offDaysUsed/dataOffDays[0].totalOffDays),
          center: new Text(
            "${(dataOffDays[0].totalOffDays - dataOffDays[0].offDaysUsed).toString()}",
            style:
            new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
          ),
          footer: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("Phép năm : ${dataOffDays[0].totalOffDaysOfYear.toString()} ngày", style: TextStyle(fontSize: 12),),
              Text("Nghỉ thực tế: ${dataOffDays[0].offDaysUsed.toString()} ngày",  style: TextStyle(fontSize: 12),),
            ],
          ),
          circularStrokeCap: CircularStrokeCap.round,
          progressColor: Colors.blue,
        ),

      ],
    ));
  }
}

