import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_project_full.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_time_sheet.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/menu/home/list_home.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/work_days.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/off_days.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/late.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/leave_early.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/work_more.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/bussiness.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/over_time.dart';
class WidgetTimeKeepingChart extends StatefulWidget {
  const WidgetTimeKeepingChart({
    Key key,
  }) : super(key: key);

  @override
  _WidgetTimeKeepingChartState createState() => _WidgetTimeKeepingChartState();
}

class _WidgetTimeKeepingChartState extends State<WidgetTimeKeepingChart>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Column(
          children: [

            Expanded(
              child: GridView.count(

                primary: false,
                shrinkWrap: true,
                childAspectRatio: (2 /2.4),
                crossAxisSpacing: 2,
                mainAxisSpacing: 2,
                crossAxisCount: 2,
                children: [
                  WidgetWorkDays(),

                  WidgetOffDays(),
                  WidgetLate(),
                  WidgetLeaveEarly(),
                  WidgetWorkMore(),
                  WidgetBussiness(),
                  WidgetOvertime(),

                ],
              ),
            ),
            SizedBox(height: 70,),

          ],


        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}
