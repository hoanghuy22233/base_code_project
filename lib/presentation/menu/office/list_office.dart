import 'package:base_code_project/model/date_not_api/data_not_api_office.dart';
import 'package:base_code_project/presentation/menu/sc_schedule.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/sc_full_timekeeping_form.dart';
import 'package:base_code_project/presentation/menu/office/internal_notification/sc_notification.dart';
import 'package:lottie/lottie.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_chart_form/sc_time_keeping_chart.dart';
import 'package:base_code_project/presentation/menu/office/propose_form/sc_full_propose_form.dart';
class WidgetListOffice extends StatefulWidget {
  @override
  _WidgetListOfficeState createState() => _WidgetListOfficeState();
}

class _WidgetListOfficeState extends State<WidgetListOffice> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataOffice.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              if (dataOffice[index].id == 0 ) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FullTimeKeepingScreen()),
                ));
              }
              else if (dataOffice[index].id == 1) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TimeKeepingChartScreen()),
                ));
              }else if (dataOffice[index].id == 2) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchedulePageScreen()),
                ));
              }else if (dataOffice[index].id == 3) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchedulePageScreen()),
                ));
              }else if (dataOffice[index].id == 4) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchedulePageScreen()),
                ));
              }else if (dataOffice[index].id == 5) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FullProposeScreen()),
                ));
              }else if (dataOffice[index].id == 6) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NotificationPage()),
                ));
              }else if (dataOffice[index].id == 7) {
                return (Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchedulePageScreen()),
                ));
              }
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          "${dataOffice[index].img}",
                          width: 20,
                          height: 20,
                        )),
                    Expanded(
                        flex: 9,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Text("${dataOffice[index].textForm}"),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}


