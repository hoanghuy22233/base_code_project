import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_waiting_form.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/widget_time_keeping_detail_appbar.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/sc_addpicture.dart';

class TimeKeepingWaitingDetailScreen extends StatefulWidget {
  final int id;
  TimeKeepingWaitingDetailScreen({Key key, this.id}) : super(key: key);

  TimeKeepingWaitingDetailScreenState createState() => TimeKeepingWaitingDetailScreenState();
}

class TimeKeepingWaitingDetailScreenState extends State<TimeKeepingWaitingDetailScreen> {
  bool isHD = false;
  bool isShow = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView(
                children: [
                  buildDetail(
                      "Người tạo:", dataTimeKeepingWaiting[widget.id].creator),
                  buildDetail(
                      "Loại đơn:", dataTimeKeepingWaiting[widget.id].name),
                  buildDetail("Ngày nộp đơn:",
                      "${dataTimeKeepingWaiting[widget.id].createdDate} ${dataTimeKeepingWaiting[widget.id].createdTime}"),
                  buildDetail("Thời gian:",
                      "${dataTimeKeepingWaiting[widget.id].date} ${dataTimeKeepingWaiting[widget.id].shift}"),
                  buildDetail("Ghi chú:",
                      "${dataTimeKeepingWaiting[widget.id].description}"),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Trạng thái phê duyệt:"),
                          Text(
                            "Chưa duyệt".toUpperCase(),
                            style: TextStyle(color: Colors.orange),
                          ),
                        ],
                      ),
                    ),
                  ),
                  buildDetail("Người duyệt:",
                      "${dataTimeKeepingWaiting[widget.id].approvedBy}"),
                  buildDetail("Người theo dõi:",
                      "${dataTimeKeepingWaiting[widget.id].supervisor}"),

                  Card(
                    child: Container(
                        height:155,

                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  "assets/images/chatting.png",
                                  width: 40,
                                  height: 40,
                                  color: Colors.blue,
                                ),
                              ),
                              Expanded(flex: 9, child: Text(" Trao đổi")),
                            ]),
                            Row(
                              children: [
                                Expanded(
                                  flex: 8,
                                  child: TextField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.zero
                                      ),
                                      labelText: 'Nhập nội dung...',

                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                    child: IconButton(
                                        icon: Icon(Icons.camera_alt, color: Colors.blue,),
                                        onPressed: () {
                                          showMaterialModalBottomSheet(
                                              context: context,
                                              builder: (context) =>
                                                  AddPictureScreen());
                                        })),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        icon: Image.asset("assets/images/direct.png", color:  Colors.blue)
                                        ))
                              ],
                            ),
                            SizedBox(height: 5,),
                            Container(

                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: Colors.red,
                              ),
                              child: Text("Xóa đơn", style: TextStyle(color: Colors.white),),
                            )
                          ],
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingDetailAppbar();

  buildDetail(String name, String detail) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
              ),
              Text(detail),
            ],
          ),
        ),
      ),
    );
  }
}
