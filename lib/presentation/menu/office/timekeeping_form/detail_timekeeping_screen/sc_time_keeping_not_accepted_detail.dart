import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_not_accepted_form.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/widget_time_keeping_detail_appbar.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/sc_addpicture.dart';

class TimeKeepingDeniedDetailScreen extends StatefulWidget {
  final int id;
  TimeKeepingDeniedDetailScreen({Key key, this.id}) : super(key: key);

  TimeKeepingDeniedDetailScreenState createState() => TimeKeepingDeniedDetailScreenState();
}

class TimeKeepingDeniedDetailScreenState extends State<TimeKeepingDeniedDetailScreen> {
  bool isHD = false;
  bool isShow = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView(
                children: [
                  buildDetail(
                      "Người tạo:", dataTimeKeepingNotAccepted[widget.id].creator),
                  buildDetail(
                      "Loại đơn:", dataTimeKeepingNotAccepted[widget.id].name),
                  buildDetail("Ngày nộp đơn:",
                      "${dataTimeKeepingNotAccepted[widget.id].createdDate} ${dataTimeKeepingNotAccepted[widget.id].createdTime}"),
                  buildDetail("Thời gian:",
                      "${dataTimeKeepingNotAccepted[widget.id].date} ${dataTimeKeepingNotAccepted[widget.id].shift}"),
                  buildDetail("Ghi chú:",
                      "${dataTimeKeepingNotAccepted[widget.id].description}"),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Trạng thái phê duyệt:"),
                          Text(
                            "Không duyệt".toUpperCase(),
                            style: TextStyle(color: Colors.red),
                          ),
                        ],
                      ),
                    ),
                  ),
                  buildDetail("Người duyệt:",
                      "${dataTimeKeepingNotAccepted[widget.id].approvedBy}"),
                  buildDetail("Người theo dõi:",
                      "${dataTimeKeepingNotAccepted[widget.id].supervisor}"),
                  buildDetail("Người phê duyệt:",
                      "${dataTimeKeepingNotAccepted[widget.id].approvedBy} - ${dataTimeKeepingNotAccepted[widget.id].reasonDenied}"),
                  buildDetail("Ngày phê duyệt:",
                      "${dataTimeKeepingNotAccepted[widget.id].approvedDate} ${dataTimeKeepingNotAccepted[widget.id].approvedTime}"),
                  Card(
                    child: Container(
                        height: 150,
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  "assets/images/chatting.png",
                                  width: 40,
                                  height: 40,
                                  color: Colors.blue,
                                ),
                              ),
                              Expanded(flex: 9, child: Text(" Trao đổi")),
                            ]),
                            Row(
                              children: [
                                Expanded(
                                  flex: 8,
                                  child: TextField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Nhập nội dung...',
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                    child: IconButton(
                                        icon: Icon(Icons.camera_alt, color: Colors.blue,),
                                        onPressed: () {
                                          showMaterialModalBottomSheet(
                                              context: context,
                                              builder: (context) =>
                                                  AddPictureScreen());
                                        })),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        icon: Image.asset("assets/images/direct.png", color:  Colors.blue)
                                        ))
                              ],
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingDetailAppbar();

  buildDetail(String name, String detail) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
              ),
              Text(detail),
            ],
          ),
        ),
      ),
    );
  }
}
