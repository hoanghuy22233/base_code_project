import 'package:flutter/material.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_addpicture.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/child_list_addpicture.dart';
class ListAddPicture extends StatefulWidget {
  @override
  _ListAddPictureState createState() => _ListAddPictureState();
}
class _ListAddPictureState extends State<ListAddPicture> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataAddPicture.length+1,
      itemBuilder: (context, index) {
        if( index == dataAddPicture.length){
          return GestureDetector
            (
            onTap: () {
              Navigator.pop(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          "assets/images/delete_red.png",
                          width: 20,
                          height: 20,
                        )),
                    Expanded(
                        flex: 9,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Text("Bỏ qua"),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        }else{
          return Container(
            // margin: EdgeInsets.symmetric(horizontal: 10),
              child: ChildListAddPicture(
                id: index
                ,
              ));
        }

      },
    );
  }
}

