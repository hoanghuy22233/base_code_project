import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/list_add_picture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/child_list_addpicture.dart';


class AddPictureScreen extends StatefulWidget {
  AddPictureScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  AddPictureScreenState createState() => AddPictureScreenState();
}
class AddPictureScreenState extends State<AddPictureScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/4 ,
      child: (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 23, vertical: 15),
                child: Text(
                  "Chọn tệp đính kèm",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.blue),
                ),
              )),
          Expanded(
            flex: 9,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child: ListAddPicture(),
            ),
          ),
        ],
      )),
    );
  }

}