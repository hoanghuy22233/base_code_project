import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_not_accepted_form.dart';
import 'package:base_code_project/presentation/menu/crm/sc_crm.dart';
import 'package:base_code_project/presentation/menu/sc_hrm.dart';
import 'package:base_code_project/presentation/menu/monitoring/sc_monitoring.dart';
import 'package:base_code_project/presentation/menu/office/sc_office.dart';
import 'package:base_code_project/presentation/menu/sc_schedule.dart';
import 'package:base_code_project/presentation/menu/work/sc_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:drop_cap_text/drop_cap_text.dart';

import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/sc_time_keeping_not_accepted_detail.dart';

class ChildListTimekeepingNotAcceptedForm extends StatelessWidget {
  final int id;
  ChildListTimekeepingNotAcceptedForm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TimeKeepingDeniedDetailScreen(id: id)),
        ),
      child: Column(

        crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 15,),
            height: 25,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey[300],
            child: Row(
              children: [
                Text("${dataTimeKeepingNotAccepted[id].name}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal:15),

            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset("assets/images/clock.png", height: 15, width: 15,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingNotAccepted[id].timeBegin}"),
                    Text(" - "),
                    Text("${dataTimeKeepingNotAccepted[id].timeEnd}")

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/flag (1).png",height: 15, width: 15, color: Colors.blue,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingNotAccepted[id].shift} - ${dataTimeKeepingNotAccepted[id].date}"),

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/question.png",height: 15, width: 15, color: Colors.green,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingNotAccepted[id].reason}"),

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/danger-sign.png", height: 15, width: 15, color: Colors.red,),
                    SizedBox(width:5,),
                    Text("${dataTimeKeepingNotAccepted[id].description}"),
                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/ic_checked.png", height: 15, width: 15),
                    SizedBox(width:5,),
                    Text("Không duyệt".toUpperCase(), style: TextStyle(color: Colors.red),)
                  ],
                ),
                DropCapText(
                  "Người duyệt: ${dataTimeKeepingNotAccepted[id].approvedBy} - ${dataTimeKeepingNotAccepted[id].reasonDenied}",
                  dropCap: DropCap(
                    width: 15, height: 15,
                    child: Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                  ),
                ),
                //

                Row(
                  children: [
                    Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    SizedBox(width:5,),


                    Text("Người theo dõi : ${dataTimeKeepingNotAccepted[id].supervisor}"),

                  ],
                ),
                Row(children: [
                  Text("(${dataTimeKeepingNotAccepted[id].supervisorEmail})")
                ],)

              ],
            ),
          ),

        ],
      )


    );
  }
}
