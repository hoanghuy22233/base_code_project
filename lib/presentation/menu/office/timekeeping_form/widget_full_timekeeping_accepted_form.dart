import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_accepted_form.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/list_timekeeping_accepted_form.dart';
import 'package:base_code_project/presentation/menu/home/list_home.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';

class WidgetFullTimekeepingAccepted extends StatefulWidget {
  const WidgetFullTimekeepingAccepted({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullTimekeepingAcceptedState createState() => _WidgetFullTimekeepingAcceptedState();
}

class _WidgetFullTimekeepingAcceptedState extends State<WidgetFullTimekeepingAccepted>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 1.3,
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      ListView.builder(
                        scrollDirection: Axis.vertical,
                        // physics:  AlwaysScrollableScrollPhysics(),
                        itemCount: dataTimeKeepingAccepted.length,
                        itemBuilder: (context, index) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 3.5,
                            // color: Colors.blue,
                            // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                            child: ChildListTimekeepingAcceptedForm(
                              id: index,
                            ),
                          );
                        },
                      ),
                      Positioned(
                          right: 10,
                          top: 250,
                          child: Image.asset(
                            "assets/images/image_add.png",
                            width: 30,
                            height: 30,
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/ic_fillter.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Expanded(flex: 6, child: Container()),
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/images/ic_list.png",
                    width: 25,
                    height: 25,
                  )),
            ],
          )
        ],
      ),
    );
    // return ListView(
    //   // width: double.infinity,
    //   // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    //   children: [
    //     Container(
    //       margin: EdgeInsets.all(8),
    //       child: Column(
    //         mainAxisAlignment: MainAxisAlignment.start,
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 1 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 2 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 3 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 4 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 5 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 6 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 7 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 8 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 9 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //         ],
    //       ),
    //     )
    //   ],
    // );
  }
}
