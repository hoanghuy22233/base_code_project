import 'package:base_code_project/model/date_not_api/data_not_api_propose_waiting_form.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/widget_time_keeping_detail_appbar.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:base_code_project/presentation/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/sc_addpicture.dart';

class ProposeWaitingDetailScreen extends StatefulWidget {
  final int id;
  ProposeWaitingDetailScreen({Key key, this.id}) : super(key: key);

  ProposeWaitingDetailScreenState createState() => ProposeWaitingDetailScreenState();
}

class ProposeWaitingDetailScreenState extends State<ProposeWaitingDetailScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView(
                children: [
                  buildDetail(
                      "Mẫu đề xuất", dataProposeWaiting[widget.id].type),
                  buildDetail(
                      "Tên đề xuất:", dataProposeWaiting[widget.id].name),
                  buildDetail("Nội dung:",
                      "${dataProposeWaiting[widget.id].description}"),
                  buildDetail("Họ tên:",
                      "${dataProposeWaiting[widget.id].target_name}"),
                  buildDetail("Bộ phận:",
                      "${dataProposeWaiting[widget.id].department}"),
                  buildDetail("Bộ phận:",
                      "${dataProposeWaiting[widget.id].department}"),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Trạng thái:"),
                          Text(
                            "Chờ duyệt".toUpperCase(),
                            style: TextStyle(color: Colors.orange),
                          ),
                        ],
                      ),
                    ),
                  ),
                  buildDetail("Người duyệt:",
                      "${dataProposeWaiting[widget.id].approvedBy}"),
                  buildDetail("Người theo dõi:",
                      "${dataProposeWaiting[widget.id].supervisor}"),
                  buildDetail("Người phê duyệt:",
                      "${dataProposeWaiting[widget.id].approvedBy}"),
                  buildDetail("Ngày phê duyệt:",
                      "${dataProposeWaiting[widget.id].approvedDate} ${dataProposeWaiting[widget.id].approvedTime}"),
                  Card(
                    child: Container(
                        height: 150,
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  "assets/images/chatting.png",
                                  width: 40,
                                  height: 40,
                                  color: Colors.blue,
                                ),
                              ),
                              Expanded(flex: 9, child: Text(" Trao đổi")),
                            ]),
                            Row(
                              children: [
                                Expanded(
                                  flex: 8,
                                  child: TextField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Nhập nội dung...',
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        icon: Icon(Icons.camera_alt, color: Colors.blue,),
                                        onPressed: () {
                                          showMaterialModalBottomSheet(
                                              context: context,
                                              builder: (context) =>
                                                  AddPictureScreen());
                                        })),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        icon: Image.asset("assets/images/direct.png", color:  Colors.blue)
                                    ))
                              ],
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingDetailAppbar();

  buildDetail(String name, String detail) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
              ),
              Text(detail),
            ],
          ),
        ),
      ),
    );
  }
}
