import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_propose_waiting_form.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/menu/office/propose_form/list_propose_Waiting_form.dart';
import 'package:base_code_project/presentation/menu/home/list_home.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';

class WidgetFullProposeWaiting extends StatefulWidget {
  const WidgetFullProposeWaiting({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullProposeWaitingState createState() => _WidgetFullProposeWaitingState();
}

class _WidgetFullProposeWaitingState extends State<WidgetFullProposeWaiting>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            height: MediaQuery.of(context).size.height / 1.3,
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 15,
                  margin: EdgeInsets.symmetric(horizontal: 17),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/img_search.png",
                        width: 15,
                        height: 15,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        flex: 8,
                        child: TextField(
                          decoration: new InputDecoration.collapsed(
                            // border: InputBorder.none,
                              hintText: 'Tìm kiếm'),
                        ),

                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 3,
                  // color: Colors.grey[300],
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      ListView.builder(
                        scrollDirection: Axis.vertical,
                        // physics:  AlwaysScrollableScrollPhysics(),
                        itemCount: dataProposeWaiting.length,
                        itemBuilder: (context, index) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 5,
                            // color: Colors.blue,
                            // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                            child: ChildListProposeWaitingForm(
                              id: index,
                            ),
                          );
                        },
                      ),
                      Positioned(
                          right: 10,
                          top: 250,
                          child: Image.asset(
                            "assets/images/image_add.png",
                            width: 30,
                            height: 30,
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/ic_fillter.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Expanded(flex: 6, child: Container()),
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/images/ic_list.png",
                    width: 25,
                    height: 25,
                  )),
            ],
          )
        ],
      ),
    );

  }
}
