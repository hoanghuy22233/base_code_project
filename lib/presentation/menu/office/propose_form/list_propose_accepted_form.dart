
import 'package:base_code_project/model/date_not_api/data_not_api_propose_accepted_form.dart';
import 'package:base_code_project/presentation/menu/office/propose_form/detail_propose_screen/sc_propose_accepted_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ChildListProposeAcceptedForm extends StatelessWidget {
  final int id;
  ChildListProposeAcceptedForm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ProposeDetailScreen(id: id)),
        ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 15,),
            height: 25,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey[300],
            child: Row(
              children: [
                Text("${dataProposeAccepted[id].name}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal:15),

            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset("assets/images/clock.png", height: 15, width: 15,),
                    SizedBox(width: 5,),
                    Text("${dataProposeAccepted[id].date}"),


                  ],
                ),





                Row(
                  children: [
                    Image.asset("assets/images/danger-sign.png", height: 15, width: 15, color: Colors.red,),
                    SizedBox(width:5,),
                    Text("${dataProposeAccepted[id].type}"),
                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    SizedBox(width:5,),


                    Text("Người đề xuất : ${dataProposeAccepted[id].proposer}"),

                  ],
                ),

                Row(
                  children: [
                    Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    SizedBox(width:5,),


                    Text("Người duyệt : ${dataProposeAccepted[id].approver}"),

                  ],
                ),


              ],
            ),
          ),

        ],
      )


    );
  }
}
