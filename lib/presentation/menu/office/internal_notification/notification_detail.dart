import 'package:base_code_project/model/date_not_api/data_not_api_internal_notification.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:base_code_project/presentation/menu/office/internal_notification/notification_appbar.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_detail.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back_two.dart';

class NotificationDetailPage extends StatefulWidget {
  final int id;
  NotificationDetailPage({Key key, this.id}) : super(key: key);
  @override
  _NotificationDetailPageState createState() => _NotificationDetailPageState();
}

class _NotificationDetailPageState extends State<NotificationDetailPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(

        child: Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              WidgetAppbarDetailPage(
                title: AppLocalizations.of(context)
                    .translate('notification_detail.appbar'),
                left: [WidgetAppbarMenuBackTwo()],
              ),
              Expanded(
                child: ListView(
                  children: [
                    buildDetail("Ban hành bởi",
                        "${dataInternalNotification[widget.id].approvedBy}"),
                    buildDetail("Lúc",
                        "${dataInternalNotification[widget.id].announcedTime} - ${dataInternalNotification[widget.id].announcedDate}"),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                            child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text("${dataInternalNotification[widget.id].title}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                        ))),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text("${dataInternalNotification[widget.id].content}",
                                  style: TextStyle(
                                       fontSize: 16)),
                            ))),
                  ],
                ),
              )
            ],
          ),
        ),

    );
  }

  buildDetail(String name, String detail) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
              ),
              Text(detail),
            ],
          ),
        ),
      ),
    );
  }
}
