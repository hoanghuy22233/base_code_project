import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:base_code_project/presentation/menu/office/internal_notification/notification_appbar.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_internal_notification.dart';
import 'package:base_code_project/presentation/menu/office/internal_notification/list_notification.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}


class _NotificationPageState extends State<NotificationPage>{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.start ,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            WidgetNotificationAppbar(),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical ,
                itemCount: dataInternalNotification.length,
                itemBuilder: (context, index){
                  return ListNotificationPage(id: index);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

}