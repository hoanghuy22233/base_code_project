import 'package:base_code_project/presentation/TimeKeepingTwo/sc_time_keeping_two.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:base_code_project/presentation/full_project_two/widget_full_project_assign.dart';
import 'package:base_code_project/presentation/full_project_two/widget_full_project_follow.dart';
import 'package:base_code_project/presentation/full_project_two/widget_full_project_two.dart';
import 'package:base_code_project/presentation/menu/home/sc_home.dart';
import 'package:base_code_project/presentation/menu/profile/sc_profile.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';


class FullProjectTwoScreen extends StatefulWidget {
  final int id;
  FullProjectTwoScreen({Key key, this.id}) : super(key: key);

  @override
  _FullProjectTwoScreenState createState() => _FullProjectTwoScreenState();
}

class _FullProjectTwoScreenState extends State<FullProjectTwoScreen>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.grey[100],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                color: Colors.grey[100],
                // height: MediaQuery.of(context).size.height * 0.1,
                child: _buildAppbar(),
              ),
              _buildTabBarMenu(),
              //_buildTabBar()
            ],
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: TabBarView(
              controller: _tabController,
              children: [
                WidgetFullProjectTwo(),
                WidgetFullProjectAssign(
                ),
                // BannerPages(
                //   drawer: _drawerKey,
                // ),
                WidgetFullProjectFollow(),
              ],
            ),
          )),
        ],
      ),
    ));
  }

  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: "Công Việc",
        right: [
          Container(
              height: double.infinity,
              padding: EdgeInsets.only(right: 10),
              child: GestureDetector(
                onTap: () {
                  // AppNavigator.navigateCart();
                },
                child: Container(
                  height: 40,
                  width: 40,
                  child: Lottie.asset(
                    'assets/lottie/34819-notification-bell.json',
                  ),
                ),
              ))
        ],
        // left: [
        //   Padding(
        //     padding: const EdgeInsets.only(bottom: 30),
        //     child: WidgetAppbarMenuBack(),
        //   )
        // ],
      );

  Widget _buildTabBarMenu() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 80, left:0, right:0),
      color: Colors.white,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        tabs: [
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 4.3),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'CỦA TÔI',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 4.3),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'TÔI GIAO',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 4.8),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'TÔI THEO DÕI',
              textAlign: TextAlign.center,
            ),
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        physics: BouncingScrollPhysics(),
        indicatorWeight: 3,
        isScrollable: true,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 20),
        indicatorColor: AppColor.GREY,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.white,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 50,
        ),
      ),
    );
  }
}
