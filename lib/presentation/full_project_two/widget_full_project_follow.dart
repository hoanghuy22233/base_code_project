
import 'package:base_code_project/model/date_not_api/data_not_api_project_full.dart';
import 'package:base_code_project/presentation/full_project_two/list_project_full_two.dart';
import 'package:flutter/material.dart';

class WidgetFullProjectFollow extends StatefulWidget {
  const WidgetFullProjectFollow({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullProjectFollowState createState() => _WidgetFullProjectFollowState();
}

class _WidgetFullProjectFollowState extends State<WidgetFullProjectFollow>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 15,
              margin: EdgeInsets.symmetric(horizontal: 17),
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/img_search.png",
                    width: 15,
                    height: 15,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: new InputDecoration.collapsed(
                          // border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),
                    // child:TextFormField(
                    //   cursorColor: Colors.black,
                    //   // keyboardType: inputType,
                    //   decoration: new InputDecoration(
                    //       border: InputBorder.none,
                    //       focusedBorder: InputBorder.none,
                    //       enabledBorder: InputBorder.none,
                    //       errorBorder: InputBorder.none,
                    //       disabledBorder: InputBorder.none,
                    //       contentPadding:
                    //       EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                    //       hintText: "tìm kiếm"),
                    // )
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 3,
              // color: Colors.grey[300],
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
            ),
            // Container(
            //   color: Colors.grey[350],
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height / 22,
            //   child: Row(
            //     children: [
            //       Container(
            //           margin: EdgeInsets.symmetric(horizontal: 17),
            //           child: Text(
            //             "Hạn hoàn thành thứ 5, 04/03/2021",
            //             style: TextStyle(fontWeight: FontWeight.bold),
            //           )),
            //     ],
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 1,
            //   color: Colors.grey[400],
            // ),
            // Container(
            //   height: MediaQuery.of(context).size.height / 2,
            //   child: Expanded(
            //     child: Stack(
            //       children: [
            //         ListView.builder(
            //           scrollDirection: Axis.vertical,
            //           // physics:  AlwaysScrollableScrollPhysics(),
            //           itemCount: dataProjectFull.length,
            //           itemBuilder: (context, index) {
            //             return Container(
            //               width: MediaQuery.of(context).size.width,
            //               height: MediaQuery.of(context).size.height / 6,
            //               // color: Colors.blue,
            //               // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
            //               child: ChildListProjectFullTwo(
            //                 id: index,
            //               ),
            //             );
            //           },
            //         ),
            //         Positioned(
            //             right: 10,
            //             top: 250,
            //             child: Image.asset(
            //               "assets/images/image_add.png",
            //               width: 30,
            //               height: 30,
            //             )),
            //       ],
            //     ),
            //   ),
            // ),
            // Row(
            //   children: [
            //     Expanded(
            //       flex: 1,
            //       child: Image.asset(
            //         "assets/images/ic_fillter.png",
            //         width: 25,
            //         height: 25,
            //       ),
            //     ),
            //     Expanded(flex: 6, child: Container()),
            //     Expanded(
            //         flex: 1,
            //         child: Image.asset(
            //           "assets/images/ic_list.png",
            //           width: 25,
            //           height: 25,
            //         )),
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
