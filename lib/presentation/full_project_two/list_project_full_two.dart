import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_project_full.dart';
import 'package:base_code_project/presentation/menu/crm/sc_crm.dart';
import 'package:base_code_project/presentation/menu/sc_hrm.dart';
import 'package:base_code_project/presentation/menu/monitoring/sc_monitoring.dart';
import 'package:base_code_project/presentation/menu/office/sc_office.dart';
import 'package:base_code_project/presentation/menu/sc_schedule.dart';
import 'package:base_code_project/presentation/menu/work/sc_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListProjectFullTwo extends StatelessWidget {
  final int id;
  ChildListProjectFullTwo({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Image.asset(
                    "${dataProjectFull[id].imgLeft}",
                    width: 20,
                    height: 20,
                    // fit: BoxFit.cover,
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${dataProjectFull[id].textReadReport}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Colors.lightBlue,
                          decoration: TextDecoration.underline,
                          decorationColor: Colors.blue,
                          decorationThickness: 2,
                          // decorationStyle: TextDecorationStyle.dashed,
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Container(
                            width: 100,
                            height: 30,
                            // padding: EdgeInsets.all(3),
                            // color: Colors.green,
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3)),
                              color: Colors.green,
                              child: Center(
                                  child: Text(
                                    "${dataProjectFull[id].textImportant}",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12),
                                  )),
                            ),
                            // child: Container(
                            //   width: 80,
                            //   height: 20,
                            //   color: Colors.green,
                            //   decoration: BoxDecoration(
                            //       border: Border.all(
                            //         color: Colors.green[500],
                            //       ),
                            //       borderRadius: BorderRadius.all(Radius.circular(20))
                            //   ),
                            //   child: Text(
                            //     "${dataProjectFull[id].textImportant}",
                            //     style: TextStyle(
                            //       color: Colors.white,
                            //     ),
                            //     textAlign: TextAlign.center,
                            //   ),
                            // ),
                          ),
                          Container(
                            width: 100,
                            height: 30,
                            // padding: EdgeInsets.all(3),
                            // color: Colors.green,
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3)),
                              color: Colors.green,
                              child: Center(
                                  child: Text(
                                    "${dataProjectFull[id].textRework}",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12),
                                  )),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.asset(
                            "${dataProjectFull[id].imgMid}",
                            width: 15,
                            height: 15,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "${dataProjectFull[id].textTime}",
                            style:
                            TextStyle(fontSize: 12, color: Colors.grey),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                    child: Image.asset(
                      "${dataProjectFull[id].imgRight}",
                      width: 20,
                      height: 20,
                    ))
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
