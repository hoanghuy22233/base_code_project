import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';

class WidgetContactDetail extends StatelessWidget {
  WidgetContactDetail({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            "CÔNG TY TNHH GIẢI PHÁP CÔNG NGHỆ - TRUYỀN THÔNG TEMIS",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/icons/ic_address.png",
                    width: 25,
                    height: 25,
                  )),
              Expanded(
                  flex: 9,
                  child: Text(": Số 10/115 Hồ Sen - Lê Chân - Hải Phòng"))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Expanded(flex: 9, child: Text(": 092 846 84 83"))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/internet.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Expanded(flex: 9, child: Text(": http://temis.vn/"))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/gmail.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Expanded(flex: 9, child: Text(": temisvietnam@gmail.com "))
            ],
          ),
        ],
      ),
    );
  }
}
