import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/entity/branch.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/contact/widget_contact_detail.dart';
import 'package:base_code_project/presentation/contact/widget_logo_contact.dart';
import 'package:base_code_project/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

import 'barrel_contact.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen>
    with AutomaticKeepAliveClientMixin<ContactScreen> {
  @override
  void initState() {
    super.initState();
    // BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Scaffold(
        body: Container(
            color: AppColor.PRIMARY_COLOR,
            child: _buildContent()),
      ),
    );
  }

  _buildContent() {
    return Column(
      children: [_buildAppbar(), Expanded(child: _buildMenu())],
    );

  }

  Widget _buildAppbar() => WidgetContactAppbar();

  Widget _buildMenu() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical:AppValue.APP_HORIZONTAL_PADDING),
        child: Column(
          children: [
            WidgetSpacer(
              height: 50,
            ),
            _buildLogo(),
            WidgetSpacer(
              height: 40,
            ),
            _buildContactDetail(),
          ],
        ),
      ),
    );
  }

  Widget _buildContactDetail() => WidgetContactDetail();

  Widget _buildLogo() => WidgetLogoContact(height: MediaQuery.of(context).size.height/4, widthPercent: 1);

  @override
  bool get wantKeepAlive => true;
}
