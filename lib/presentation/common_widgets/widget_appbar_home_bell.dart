import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class WidgetAppbarHomeBell extends StatelessWidget {
  final Function onTap;

  const WidgetAppbarHomeBell({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: AppValue.ACTION_BAR_HEIGHT * 0.2),
        width: AppValue.ACTION_BAR_HEIGHT * 0.8,
        height: AppValue.ACTION_BAR_HEIGHT * 0.8,
        child: WidgetAppbarHome(
          icon: Lottie.asset('assets/lottie/34819-notification-bell.json'),
          onTap: onTap ??
              () {
                AppNavigator.navigateAddProject();
              },
        )
    );
  }
}
