class DataTimeKeepingNotAccepted{
  final String name;
  final String date;
  final String reason;
  final String shift;
  final String creator;
  final String timeBegin;
  final String timeEnd;
  final String description;
  final String reasonDenied;
  final String approvedBy;
  final String supervisor;
  final String supervisorEmail;
  final String createdDate;
  final String createdTime;
  final String approvedDate;
  final String approvedTime;

  DataTimeKeepingNotAccepted({this.name, this.date, this.reason, this.shift, this.creator, this.timeBegin, this.timeEnd, this.description, this.reasonDenied, this.approvedBy, this.supervisor, this.supervisorEmail, this.createdDate, this.createdTime, this.approvedDate, this.approvedTime});



}
List<DataTimeKeepingNotAccepted> dataTimeKeepingNotAccepted = [
  DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),
  DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),DataTimeKeepingNotAccepted(
      creator: 'Nguyễn Bùi Tuấn Anh',
      name: "Đơn xin vắng mặt",
      createdDate: '05/03/2021',
      createdTime: '05:15',
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonDenied: "Lý do không hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      approvedDate: "05/03/2021",
      approvedTime: "07:14"
  ),

];