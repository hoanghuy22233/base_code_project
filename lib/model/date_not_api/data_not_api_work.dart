import 'package:flutter/cupertino.dart';

class DataWork {
  final String textForm, img;
  final int id;
  DataWork({
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataWork> dataWork = [
  DataWork(
    textForm: "Công việc",
    img: "assets/images/ic_time_sheet.png",
    id: 0,
  ),
  DataWork(
    textForm:"Dự án",
    img: "assets/images/ic_job.png",
    id: 1,
  ),
  DataWork(
    textForm: "WorkFlow",
    img: "assets/images/ic_work_flow.png",
    id: 2,
  ),
];
