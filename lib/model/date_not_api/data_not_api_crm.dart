import 'package:flutter/cupertino.dart';

class DataCrm {
  final String textForm, img;
  final int id;
  DataCrm({
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataCrm> dataCrm = [
  DataCrm(
    textForm: "Chăm sóc khách hàng",
    img: "assets/images/ic_take_care.png",
    id: 0,
  ),
  DataCrm(
    textForm:"Yêu cầu hỗ trợ",
    img: "assets/images/ic_setting.png",
    id: 1,
  ),
  DataCrm(
    textForm: "Cơ hội bán hàng",
    img: "assets/images/ic_bank.png",
    id: 2,
  ),
  DataCrm(
    textForm: "khách hàng",
    img: "assets/images/ic_people.png",
    id: 3,
  ),
];
