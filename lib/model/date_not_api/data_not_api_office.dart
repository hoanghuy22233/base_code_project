import 'package:flutter/cupertino.dart';

class DataOffice {
  final String textForm,img;
  final int id;

  // final String textFormTS,
  //     textTimeSheet,
  //     textEnterTS,
  //     textMeetingSC,
  //     textPayroll,
  //     textOffer,
  //     textNotified,
  //     textBooking;

// final int backgroundColor;
  DataOffice({
    // this.textFormTS,
    // this.textTimeSheet,
    // this.textEnterTS,
    // this.textMeetingSC,
    // this.textPayroll,
    // this.textOffer,
    // this.textNotified,
    // this.textBooking,
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataOffice> dataOffice = [
  DataOffice(
    textForm: "Đơn chấm công",
    img: "assets/images/ic_form_ts.png",
    id: 0,
  ),
  DataOffice(
    textForm: "Bảng chấm công",
    img: "assets/images/ic_calender.png",
    id: 1,
  ),
  DataOffice(
    textForm: "Nhập công cho nhân viên",
    img: "assets/images/ic_calender.png",
    id: 2,
  ),
  DataOffice(
    textForm: "Lịch họp",
    img: "assets/images/ic_calendar.png",
    id: 3,
  ),
  DataOffice(
    textForm: "Bảng lương",
    img: "assets/images/ic_money.png",
    id: 4,
  ),
  DataOffice(
    textForm: "Đề xuất",
    img: "assets/images/ic_responsibility.png",
    id: 5,
  ),
  DataOffice(
    textForm: "Thông báo nội bộ",
    img: "assets/images/ic_email.png",
    id: 6,
  ),
  DataOffice(
    textForm: "Booking",
    img: "assets/images/ic_booking.png",
    id: 7,
  ),
];
