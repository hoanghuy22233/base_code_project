import 'package:base_code_project/model/api/response/barrel_response.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'rank_response.g.dart';

@JsonSerializable()
class RankResponse extends BaseResponse {
  RankData data;

  RankResponse(this.data);

  factory RankResponse.fromJson(Map<String, dynamic> json) =>
      _$RankResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RankResponseToJson(this);

}
